import * as THREE from 'three'

const _instance = new THREE.LoadingManager()

_instance.onProgress = function (url, Loaded, total) {
    console.log(url, Loaded, total)
}
export default _instance